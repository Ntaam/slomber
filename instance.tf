resource "aws_instance" "demo_instance_1" {
  count         = 2
  ami           = "ami-0aa7d40eeae50c9a9"
  subnet_id = aws_subnet.private[count.index].id


  # subnet_id     = element(aws_subnet.private.id, count.index)   
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld[count.index]"
  }
}