# resource "aws_subnet" "pub" {
#   count = 2
#   vpc_id     = aws_vpc.demo.id
#   cidr_block = element(var.aws_subnet-public, count.index)
#   availability_zone = element(var.sub_az,count.index)

#   tags = {
#     Name = "pub[count.index]"
#  }
# }

resource "aws_subnet" "pub" {
  count             = 2
  vpc_id            = aws_vpc.demo.id
  cidr_block        = cidrsubnet(var.cidr_block, 8, count.index)
  availability_zone = element(var.sub_az, count.index)


  tags = {
  "name" = "subnet ${count.index + 0}" }

}


# resource "aws_subnet" "private" {
#   count = 4
#   vpc_id     = aws_vpc.demo.id
#   cidr_block = element(var.aws_subnet-private, count.index)
#   availability_zone = element(var.sub_az,count.index)

#   tags = {
#     Name = "private[count.index]"
#  }
# }

resource "aws_subnet" "private" {
  count             = 4
  vpc_id            = aws_vpc.demo.id
  cidr_block        = cidrsubnet(var.cidr_block, 8, count.index + 4)
  availability_zone = element(var.sub_az, count.index + 4)


  tags = {
  "name" = "subnet ${count.index + 0}" }

}

resource "aws_db_subnet_group" "default" {
  count = 4
  #subnet_ids = ["${join(",", aws_subnet.private.*.id)}"]
  subnet_ids = flatten([aws_subnet.private.*.id, aws_subnet.pub.*.id])

  tags = {
    Name = "${format("My DB subnet group-%d", count.index + 1)}"

  }
}

