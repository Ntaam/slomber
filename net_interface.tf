resource "aws_network_interface" "demo_eni" {
  count = 2
  subnet_id       = element(aws_subnet.pub.*.id, count.index)
#   private_ips     = ["10.0.0.50"]
  security_groups = [aws_security_group.allow_tls.id]

  attachment {
    instance     = "aws_instance.demo_instance_1.*.id"
    device_index = "2"
  }

   tags = {
  "name" = "subnet ${count.index + 0}" }
}