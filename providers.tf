terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

data "aws_availability_zones" "available" {
  state                  = "available"
  all_availability_zones = true
}

resource "aws_networkfirewall_rule_group" "wall" {
  capacity = 1000
  name     = "demo_fire"
  type     = "STATELESS"
  rule_group {
    rules_source {
      stateless_rules_and_custom_actions {
        stateless_rule {
          priority = 5
          rule_definition {
            actions = ["aws:pass"]
            match_attributes {
              source {
                address_definition = "10.0.0.0/8"
              }
              source {
                address_definition = "192.168.0.0/16"
              }
            }
          }
        }
      }
    }
  }
}
resource "aws_networkfirewall_firewall_policy" "policy" {
  name = "demo_policy"
  firewall_policy {
    stateless_default_actions = ["aws:drop"]
    stateless_fragment_default_actions = ["aws:drop"]
    stateless_rule_group_reference {
      priority     = 20
      resource_arn = aws_networkfirewall_rule_group.wall.arn
    }
  }
}
resource "aws_networkfirewall_firewall" "fire" {
  firewall_policy_arn = aws_networkfirewall_firewall_policy.policy.arn
  name                = "example"
  vpc_id              = "aws_vpc.demo.id"
  subnet_mapping {
    subnet_id          = "aws_subnet.firewall.id"
  }
}
resource "aws_network_interface" "firewall" {
  subnet_id       = "aws_subnet.private.id"
}
resource "aws_network_interface" "application" { 
  subnet_id = "aws_subnet.pub.id"
}
# data "aws_network_interface" "firewall" { 
#   id = "eni-08592edbb0fed0e12"
# }
# data  "aws_network_interface" "application" { 
#   id = "eni-0fbad79962a1643f7"
# }
# resource "aws_route_table" "application" {
#   vpc_id = aws_vpc.example.id
#   route {
#     cidr_block           = "0.0.0.0/0"
#     # network_interface_id = data.aws_network_interface.application.id
#   }
# }  
# }
# resource "aws_route_table_association" "application" {
#   route_table_id = aws_route_table.application.id
#   subnet_id      = aws_subnet.application.id
# }
# resource "aws_route_table" "gateway" {
#   vpc_id = aws_vpc.example.id
#   route {
#     cidr_block           = aws_subnet.application.cidr_block
#     network_interface_id = data.aws_network_interface.firewall.id
#   }
# }
# resource "aws_route_table_association" "gateway" {
#   gateway_id     = aws_internet_gateway.example.id
#   route_table_id = aws_route_table.gateway.id
# }