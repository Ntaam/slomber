resource "aws_lb" "external" {
  name               = "demo"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_tls.id]
  subnets            = [for subnet in aws_subnet.pub : subnet.id]

  enable_deletion_protection = false

  #   access_logs {
  #     bucket  = aws_s3_bucket.lb_logs.bucket
  #     prefix  = "test-lb"
  #     enabled = true
  #   }

  tags = {
    Environment = "production"
  }


}
resource "aws_lb_target_group" "target_alb" {
  name        = "tf-example-lb-alb-tg"
  target_type = "alb"
  port        = 443
  protocol    = "HTTPS"
  vpc_id      = aws_vpc.demo.id
}

resource "aws_lb_target_group_attachment" "pub" {
  count            = 2
  target_group_arn = aws_lb_target_group.target_alb.arn
  target_id        = aws_instance.demo_instance_1[count.index].id
  port             = 80
  depends_on = [
    aws_instance.demo_instance_1,
  ]
}


# resource "aws_lb_target_group_attachment" "pub_2" {
#   target_group_arn = aws_lb_target_group.external-alb.arn
#   target_id        = aws_instance.test.id
#   port             = 80
# }

resource "aws_lb_listener" "external-alb" {
  load_balancer_arn = aws_lb.external.arn
  port              = "443"
  protocol          = "HTTPS"
  #   ssl_policy        = "ELBSecurityPolicy-2016-08"
  #   certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_alb.arn

  }
}

