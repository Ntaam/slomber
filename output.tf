output "lb_dns_name" {
  description = "The DNS name of the load balancer"
  value       = aws_lb.external.dns_name
}


output "eni" {
    
    description = "eni for demo_vpc workload"
    value = aws_network_interface.demo_eni.*.id
  
}